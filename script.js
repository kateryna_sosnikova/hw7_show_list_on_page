
const array = ['hello', 'world', 'Kiev', 'Kharkiv', [1, 'Nikolaev', 'Bye'], 'Odessa', { name: 'newlist', prop: 'hjjj'},'Lviv'];
const array2 = ['1', '2', '3', 'sea', 'user', { a: 150, b: 'name' }, 23];

let showList = (array) => {
    let list = document.createElement('ol');
    document.body.append(list);
    let newList = array.map(function (item) {
        if (Array.isArray(item)) {
            let subList = item.map(function (subitem) {
                return `<li>${subitem}</li>`;
            });
            return `<li><ol>${subList.join('')}</ol></li>`;
        }
        else if (typeof item === 'object') {
            let subObjList = '';
            for (let subItem in item) {
                subObjList+=`<li>${subItem}: ${item[subItem]}</li>`
            }
            return `<li><ul>${subObjList}</ul></li>`;
        }
        return `<li>${item}</li>`;
    });
    list.innerHTML = newList.join('');
}

window.addEventListener('load', () => {
    showList(array);
    showTimer(10);
});

let showTimer = (sec) => {
    let container = document.createElement('div');
    document.body.append(container);
    container.innerHTML = 'Time left:';
    let timer = document.createElement('span');
    container.append(timer);
    timer.innerHTML = sec; 
    let timeLeft = +(timer.innerHTML);

    let timerFunc = setInterval(() => {
        if (timeLeft >= 0) {
            timer.innerHTML = timeLeft--;
        } else {
            document.body.innerHTML = '';
            clearInterval(timerFunc);
        }
    }, 1000)
}

/*
function listArray(array) {
    let list = document.createElement('ol');
    document.body.append(list);

    for (let i = 0; i < array.length; i++) {
        let item = document.createElement('li');

        if (typeof array[i] === 'object') {
            let subList = document.createElement('ol');
            for (let key in array[i]) {
                let subItem = document.createElement('li');
                subItem.textContent = array[i][key];
                subList.append(subItem);
            }
            item.appendChild(subList);
        } else {
            item.textContent = array[i];
        }
        list.append(item);
    }
} 
listArray(array); */
